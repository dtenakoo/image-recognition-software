import cv2
import numpy as np
import math

img = cv2.imread("gelpack_ASIC_table_pic_slanted.png")
grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(grey, 75, 150)

lines = cv2.HoughLinesP(edges, 1, np.pi/180, 10, maxLineGap=0) # Calculating the lines
print(lines) # Print the lines

points_x1 = open("x1pnt.txt", "a+")
points_x1.truncate(0)
points_x2 = open("x2pnt.txt", "a+")
points_x2.truncate(0)

points_y1 = open("y1pnt.txt", "a+")
points_y1.truncate(0)

points_y2 = open("y2pnt.txt", "a+")
points_y2.truncate(0)

# Draw the lines
for line in lines:
    x1, y1, x2, y2 = line[0]
    x1s = str(x1)
    x2s = str(x2)
    y1s = str(y1)
    y2s = str(y2)

    points_x1.write(x1s + "\n")
    points_x2.write(x2s + "\n")
    points_y1.write(y1s + "\n")
    points_y2.write(y1s + "\n")

    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 2)

points_x1.close()
cv2.imshow("Edges", edges)
cv2.imshow("Image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()

points_x1.close()
points_x2.close()
points_y1.close()
points_y2.close()

print("----BREAK INFO----")

# Find the size of the data sheet
x1_size = open("x1pnt.txt", "r")

size = 0

for i in x1_size.readlines():
    size += 1

x1_size.close()

print("The size of the array:")
print(size)

print("----BREAK INFO---")

# Define an array with the given size so we can transform the text file into an array
x1_array = [None] * (size)
x2_array = [None] * (size)
y1_array = [None] * (size)
y2_array = [None] * (size)

# Convert the text into an array for x1
x1Read = open("x1pnt.txt", "r")

for k, line in enumerate(x1Read):
    line = int(line)
    x1_array[k] = line

x1Read.close()

# Convert the text into an array for x2
x2Read = open("x2pnt.txt", "r")

for o, word in enumerate(x2Read):
    word = int(word)
    x2_array[o] = word

x2Read.close()

# Convert the text into an array for y1
y1Read = open("y1pnt.txt", "r")

for l, list in enumerate(y1Read):
    list = int(list)
    y1_array[l] = list

y1Read.close()

# Convert the text into an array for y2
y2Read = open("y2pnt.txt", "r")

for k, num in enumerate(y2Read):
    num = int(num)
    y2_array[k] = num

y2Read.close()

# Find the left and right points of the table (top of table)
x1Loc = min(x1_array)

indY = x1_array.index(x1Loc)

y1Loc = y1_array[indY]

# -------------------- #

y2Loc = min(y2_array)

indX = y2_array.index(y2Loc)

x2Loc = x2_array[indX]

xDiff = x2Loc - x1Loc
yDiff = y2Loc - y1Loc

# Calculate angles and round
pi = 22/7
ratio = yDiff / xDiff
angleRad = math.atan(ratio)
angleDeg = angleRad*(180/pi)

x1Loc = round(x1Loc)
x2Loc = round(x2Loc)
y1Loc = round(y1Loc)
y2Loc = round(y2Loc)



print("x1 postion is: " + str(x1Loc) + "px")
print("x2 postion is: " + str(x2Loc) + "px")
print("y1 postion is: " + str(y1Loc) + "px")
print("y2 postion is: " + str(y2Loc) + "px")

print("---BREAK INFO---")

# Calculate the width and height using num_col and num_row
widthT = (x2Loc - x1Loc)
heightT = (y2Loc - y1Loc)



print("The width of the table is: " + str(widthT) + "px")
print("The height of the table is: " + str(heightT) + "px")

print("---BREAK INFO---")
print("angle " + str(angleDeg))

image = cv2.imread('pre.png', 0)
thresh = 255 - cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

rotate = cv2.imread("gelpack_ASIC_table_pic_slanted.png", 0)
rows, cols = image.shape

matrixR = cv2.getRotationMatrix2D((rows/2, cols/2), angleDeg, 1)
new_img = cv2.warpAffine(rotate, matrixR, (cols, rows), borderValue=(255, 255, 255))

# Screenshot the the rotated image



cv2.imshow("output", new_img)
cv2.imwrite("gelpack_ASIC_table_pic_corrected.png", new_img)

cv2.waitKey()

# Does not work because the starting position is incorrect: TEST 16 will rotate, then screenshot a better image, then
# will edit that screenshot using test14


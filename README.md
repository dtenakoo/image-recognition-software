The first version of working code is named markI.py
The second version of working code is named markII.py (MOST ACCURATE)

I have created two idealized images of the tables. The reason I created these
idealized images to test on is because if we cannot get the software to work
on an ideal level, then there is no point in developing for non-ideal 
situations first. Therefore, testing on these ideal images is the way to start.

-------------------------------
Theory of code:
1. We open and save the image into a variable and want to transform it into a 
grey image. This is because when working with the full coloured image, it has 
colours ranging from 0 to 255 and this takes a lot of computer power 
to analyze full-coloured images. When we convert to grey images, it ranges from
0 to 7 using less processing power.

2. We then use either the HoughLines method or HoughLines Probablity method to 
identify points on the image and save all those points to a text file.

3. We then draw the points as lines so we can see the lines that have been 
scanned.

4. Because the lines are not always properly scanned, we need to make sure we 
only get the starting position of start cell of the table (top left cell). We do
this by getting the smallest x1, and y1 values of the recorded data. 

5. We then get the width & height of each cell(method explained below). Since we
know the starting postion and the width/height of each cell (in pixels), we can 
itterate and add width and height to the starting postion to get the 
position of each individual cell as required.

6. We read the text in each cell using the Tesseract library and save the text 
either into an array or text file.

7. Create buttons with each of the text entries on the buttons. (GUI) 
-------------------------------

-------------------------------
Mark I CODE:
This code uses the HoughLines method which records each detected point of the 
table into polar coordinates. The code then changes the polar coordinates to 
cartesian coordinates and we save this data into text files. We then draw lines
using the coordinates to see which lines have been scanned and display this. 

SEE IMAGES:
Image being scanned: "made.png"
Image of lines that were scanned: "made_scanned.png"

Not all lines were scanned properlly. 
The starting postion(first cell in table) was located by finding the smallest 
x1 and y1 value. The width and height of each cell was found by locating the 
second smallest x1 and y1 values.

Then, the code will itterate for the first three cells to add the width to the 
starting position and it will take an idividual screenshot of those cells, save 
it into a variable, then use the PyTesseract library to read the text in that
individual screenshot. This data is then displayed for the user.

NOTES:
This code isn't too stable as it works for the "made.png" image but does not
work for the "ideal.png." Not all of the lines are scanned and the ones that are,
they are not scanned properly (for the ideal.png)
You can also see the made_scanned.png and notice that not all lines are properly
scanned.
-------------------------------

-------------------------------
Mark II CODE:
This code uses the HoughLines Probability method which detects points and makes
an educated guess as to how they should continue with respect to how a line 
should behave (uses slope and y-intercept calculations).

The image is changed to a grey image and scanned. Then data points are saved 
into a text file. To see the scanned lines, we take the data points 
(x1, y1) & (x2, y2) and we draw lines using these points.

SEE IMAGES:
Image being scanned: "ideal.png"
Image of lines that were scanned: "ideal_scanned.png"

Because this method gives points within the image (does not give points outside 
the image like Mark I code using HoughLines method), we use a different way to 
find the width and height of each cell. We find the starting postion of the cell
by finding the smallest x1 and y1 values. Then to find the wdith and height, we
find the largest x2 and y2 values. Now, we know the width of the entire table is 
x2 - x1 and the height of the table is y2-y1. We know the number of rows and 
columns to 9x7 (2 images given by CERN had only this many rows and columns so 
it was assumed that upcoming images will also have the same number of rows
and columns) so by dividing the width of the table by the number of columns gave
the width of each cell and dividing the height of the table by the number of 
rows gave the height of each cell.

Then, I ittereated for the number of rows and columns and added the width
and height to the starting postion as required to create a varible that creates 
an individual screenshot/picture of each corresponding cell. Each cell was
scanned and read and the text in it was saved to a matrix and displayed to the 
user.

SEE IMAGES:
The output of the scanned image: "markII_output.png"
-------------------------------

-------------------------------
DISCLAIMER:
These programs are not perfect and by no means are meant to be. I am trying to 
make progress in this field and this is the current state of what I have. I will
continue to improve on what I have but it will not be perfect. I am not a 
professional programmer, nor do I call myself one. I am second year Space Science
student at York University trying to make progress in this field. That is all.
-------------------------------

-------------------------------
TEST PROGRAMS:
Any files with the name "testX.py" are source codes that I have used and tried 
to edit/work with. They don't all work but I have documented them. The source
codes were found online on Youtube tutorials or GitHub and other sites.
-------------------------------

-------------------------------
NEXT STEPS:
1. Mess and edit with threshold levels to change the thickness of detection
of lines.
2. Create a program that can take a slanted image and crop it to be parallel to
the lines of the table.
-------------------------------
import cv2
import numpy as np
import pytesseract

global num_row
num_row = 9
global num_col
num_col = 7

img = cv2.imread("gelpack_ASIC_table_pic_ideal.png")
grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(grey, 75, 150)

lines = cv2.HoughLinesP(edges, 1, np.pi/180, 10, maxLineGap=0) # Calculating the lines
print(lines) # Print the lines

points_x1 = open("x1pnt.txt", "a+")
points_x1.truncate(0)
points_x2 = open("x2pnt.txt", "a+")
points_x2.truncate(0)

points_y1 = open("y1pnt.txt", "a+")
points_y1.truncate(0)

points_y2 = open("y2pnt.txt", "a+")
points_y2.truncate(0)

# Draw the lines
for line in lines:
    x1, y1, x2, y2 = line[0]
    x1s = str(x1)
    x2s = str(x2)
    y1s = str(y1)
    y2s = str(y2)

    points_x1.write(x1s + "\n")
    points_x2.write(x2s + "\n")
    points_y1.write(y1s + "\n")
    points_y2.write(y1s + "\n")

    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 2)

points_x1.close()
cv2.imshow("Edges", edges)
cv2.imshow("Image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()

points_x1.close()
points_x2.close()
points_y1.close()
points_y2.close()

print("----BREAK INFO----")

# Find the size of the data sheet
x1_size = open("x1pnt.txt", "r")

size = 0

for i in x1_size.readlines():
    size += 1

x1_size.close()

print("The size of the array:")
print(size)

print("----BREAK INFO---")

# Define an array with the given size so we can transform the text file into an array
x1_array = [None] * (size)
x2_array = [None] * (size)
y1_array = [None] * (size)
y2_array = [None] * (size)

# Convert the text into an array for x1
x1Read = open("x1pnt.txt", "r")

for k, line in enumerate(x1Read):
    line = int(line)
    x1_array[k] = line

x1Read.close()

# Convert the text into an array for x2
x2Read = open("x2pnt.txt", "r")

for o, word in enumerate(x2Read):
    word = int(word)
    x2_array[o] = word

x2Read.close()

# Convert the text into an array for y1
y1Read = open("y1pnt.txt", "r")

for l, list in enumerate(y1Read):
    list = int(list)
    y1_array[l] = list

y1Read.close()

# Convert the text into an array for y2
y2Read = open("y2pnt.txt", "r")

for k, num in enumerate(y2Read):
    num = int(num)
    y2_array[k] = num

y2Read.close()

# Find the max and min points of the arrays
x1Loc = min(x1_array)
x2Loc = max(x2_array)
y1Loc = min(y1_array)
y2Loc = max(y2_array)

x1Loc = round(x1Loc)
x2Loc = round(x2Loc)
y1Loc = round(y1Loc)
y2Loc = round(y2Loc)

print("x1 postion is: " + str(x1Loc) + "px")
print("x2 postion is: " + str(x2Loc) + "px")
print("y1 postion is: " + str(y1Loc) + "px")
print("y2 postion is: " + str(y2Loc) + "px")

print("---BREAK INFO---")

# Calculate the width and height using num_col and num_row
width = (x2Loc - x1Loc) / (num_col)
height = (y2Loc - y1Loc) / (num_row)

width = round(width)
height = round(height)

print("The width of each cell is: " + str(width) + "px")
print("The height of each cell is: " + str(height) + "px")

print("---BREAK INFO---")

# Scan and read the text in each cell
image = cv2.imread('gelpack_ASIC_table_pic_ideal.png', 0)
thresh = 255 - cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

# Create a matrix 9x7
# matrix = np.empty([num_row, num_col], int)

# for i in range(num_row):
#    for j in range(num_col):
#        matrix[i][j] = 1

matrix = [
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"],
    ["1", "1", "1", "1", "1", "1", "1"]
]

for row in range(num_row):
    y, h = (y1Loc + (height*row) + 6), height - 9
    for col in range(num_col):
        x, w = (x1Loc + (width*col) + 4), width - 7
        ROI1 = thresh[y:y + h, x:x + w]
        value = pytesseract.image_to_string(ROI1, lang='eng', config='--psm 6')
        if (value == "Oo"):
            value = ""
            matrix[row][col] = value
        else:
            # value = int(value)
            matrix[row][col] = value


x,y,w,h = x1Loc, y1Loc, width, height

print(matrix)

cv2.imshow('thresh', thresh)
cv2.imshow('ROI1', ROI1)
cv2.waitKey()
import cv2
import numpy as np
import pytesseract


img = cv2.imread("made.png")
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 50, 250, apertureSize=3)
lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)

pointsx1 = open("pointsx1.txt", "a+")
pointsx1.truncate(0)
pointsy1 = open("pointsy1.txt", "a+")
pointsy1.truncate(0)
pointsx2 = open("pointsx2.txt", "a+")
pointsx2.truncate(0)
pointsy2 = open("pointsy2.txt", "a+")
pointsy2.truncate(0)


for i in lines:
    rho,theta = i[0]
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho

    x1 = int(x0 + 1000*(-b))
    x1s = str(x1)

    y1 = int(y0 + 1000*(a))
    y1s = str(y1)

    x2 = int(x0 - 1000*(-b))
    x2s = str(x2)

    y2 = int(y0 - 1000*(a))
    y2s = str(y2)

    # print(x1, y1, x2, y2)

    pointsx1.write(x1s+"\n")
    pointsy1.write(y1s + "\n")
    pointsx2.write(x2s + "\n")
    pointsy2.write(y2s + "\n")

    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)

pointsx1.close()
pointsy1.close()
pointsx2.close()
pointsy2.close()

cv2.imshow("image", img)
k = cv2.waitKey(0)
cv2.destroyAllWindows()

x1Size = open("pointsx1.txt", "r")

size = 0

for i in x1Size.readlines():
    size += 1

x1_Array = [None] * (size)
x1Main = [None] * (size)

x1Size.close()

x1Read = open("pointsx1.txt", "r")

for k, line in enumerate(x1Read):
    line = int(line)
    x1_Array[k] = line

print(x1_Array)
x1Read.close()

for j in range(size):
    if x1_Array[j] >=0:
        x1Main[j] = x1_Array[j]     # Clean up x1_Array and x1Main
    if x1Main[j] == None:
        x1Main[j] = 100000

print(x1Main)
x1Pos = min(x1Main)
print(x1Pos)

for l in range(size):
    if x1Main[l] == x1Pos:
        x1Main[l] = 100000

print(x1Main)
x2Pos = min(x1Main)
print(x2Pos)

# Find the first and second minimum of the y-coordinates
y1_Array = [None] * (size)

y1Read = open("pointsy1.txt", "r")

for u, word in enumerate(y1Read):
    word = int(word)
    y1_Array[u] = word

y1Read.close()

y1Pos = min(y1_Array)
print(y1Pos)

for p in range(size):
    if y1_Array[p] == y1Pos:
        y1_Array[p] = 100000

y2Pos = min(y1_Array)
print(y2Pos)

# Find the width and height of each box (in pixels)
width = x2Pos - x1Pos
height = y2Pos - y1Pos

# x and y increments that work (through testing)
x1Pos = x1Pos + 2
y1Pos = y1Pos + 4

# Scan and read the first three entries
image = cv2.imread('made.png', 0)
thresh = 255 - cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]



x,y,w,h = x1Pos, y1Pos, width, height
ROI1 = thresh[y:y+h,x:x+w]
data1 = pytesseract.image_to_string(ROI1, lang='eng',config='--psm 6')

x,y,w,h = x1Pos+width, y1Pos, width, height
ROI2 = thresh[y:y+h,x:x+w]
data2 = pytesseract.image_to_string(ROI2, lang='eng',config='--psm 6')

x,y,w,h = x1Pos+width+width, y1Pos, width, height
ROI3 = thresh[y:y+h,x:x+w]
data3 = pytesseract.image_to_string(ROI3, lang='eng',config='--psm 6')

if not data3:
    data3 = "0"

print("------Data------")
print(data1)
print(data2)
print(data3)

cv2.imshow('thresh', thresh)
cv2.imshow('ROI1', ROI1)
cv2.imshow('ROI2', ROI2)
cv2.waitKey()
